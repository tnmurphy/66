BIN_TARGETS := \
66-scandir \
66-scanctl \
66-init \
66-tree \
66-dbctl \
66-enable \
66-disable \
66-parser \
66-start \
66-stop \
66-svctl \
66-all \
66-intree \
66-inresolve \
66-inservice \
66-instate \
66-env \
66-boot \
66-shutdown \
66-shutdownd \
66-hpr \
66-update \
66-umountall \
66-echo \
execl-envfile \
66-nuke

LIB_DEFS := 66=66
